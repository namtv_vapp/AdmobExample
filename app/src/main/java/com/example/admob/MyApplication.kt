package com.example.admob

import android.app.Application
import com.vapp.admoblibrary.AdmodUtils
import com.vapp.admoblibrary.AppOpenManager

class MyApplication :Application() {
    var appOpenManager: AppOpenManager? = null
    override fun onCreate() {
        //Lúc khởi động app cho khởi tạo 1 đối tượng AppOpenManager, truyền id app open on resume vào
        AdmodUtils.ads_admob_open_id = "ca-app-pub-3940256099942544/3419835294"
        appOpenManager = AppOpenManager(this, "ca-app-pub-3940256099942544/3419835294")
        super.onCreate()
    }
}